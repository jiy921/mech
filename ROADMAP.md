# Mech Project Roadmap

### Stage 1 - Alpha v0.0.1

Conception: started 2014
Implementation: started 2018

### Stage 2 - Beta v0.1.0

Target release: October 2022

### Stage 3 - Stable v1.0.0

Target release: ???

## Version 0.1.0 Beta

[x] Errors
[x] Distribution
[x] Paralell operators
[ ] GPU Processing
[ ] Automatic Differentiation
[x] Async blocks
[ ] Capability system
[ ] User defined functions
[x] Units
[x] Type checking
[x] Multiple dispact
[x] JIT Block Compiler
[ ] Persistence
[ ] Time travel debugging
[x] Native executables
[x] REPL
- Machines
    [ ] JSON
    [ ] Sockets
    [ ] Serial
    [ ] Bluetooth
- Editor
    [ ] Database Explorer
    [ ] API endpoint server
    [ ] Source render / edit
- Docs
    - Mech platform references
        [ ] Syntax
        [ ] Ecosystem
        [ ] Architecture
    - User Guides
        [ ] Tutorials
        [ ] How-Tos
        [ ] Mech for X 
- Website
    [ ] Homepage
    [ ] Blog
    [ ] Try

## Version 0.0.6 Alpha

[x] Testing
- Machines 
    - HTML
        [x] Mouse
        [x] Keyboard
        [x] Canvas Drawing
    [x] File I/O
    [x] Standard Streams
    [x] Strings
    [x] Number Literals
    [x] HTTP
    [x] Math
    [x] Stats
    [x] Random
    [x] Time
    [x] Set
    - System
        [x] Input Arguments
        [x] Exit
    [x] Mech Compiler
    [x] Table
